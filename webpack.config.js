const path = require('path');

  module.exports = {
    entry: './src/index.js',
    output: {
      path: path.resolve('build'),
      publicPath: '/public/assets/',
      filename: 'bundle.js',
    },
    devServer: {
      contentBase: 'public'
    },

    module: {

      rules: [
        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader'
          ]
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [
            'file-loader'
          ]
        },
       {
         test: /\.(woff|woff2|eot|ttf|otf)$/,
         use: [
           'file-loader'
         ]
       }
      ]
    }
  };